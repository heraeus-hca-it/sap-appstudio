sap.ui.define([
    "sap/ui/model/json/JSONModel"
], function (JSONModel) {
    "use strict";
    var ViewModel = JSONModel.extend("mysapui5app1.model.ViewModel", {
        constructor: function (oData){
            JSONModel.apply(this, arguments);
        }
    });
    return ViewModel;
}
);