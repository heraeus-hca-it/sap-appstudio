sap.ui.define([
    "sap/ui/core/format/DateFormat"
] , function (DateFormat) {
    "use strict";

    return {
        
        jsonDate: function(oValue, sFormat){
			  
            if (typeof oValue === 'string') {
                oValue  = eval(oValue.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
            }
            if(!sFormat||!oValue) {
                return oValue;

            }
            var sCurrentLocale = sap.ui.getCore().getConfiguration().getLanguage();

            var sTimeZoneShort = "";
            
            var dJan1 = new Date(oValue.getFullYear(), 0, 1);
            var dJul1 = new Date(oValue.getFullYear(), 6, 1);
            var dlsTimeOffset = Math.max(dJan1.getTimezoneOffset(), dJul1.getTimezoneOffset());
            if (dlsTimeOffset !== 0 && oValue.getTimezoneOffset() === dJul1.getTimezoneOffset()){
                sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.dlsTimeZoneShort");
                sFormat = sFormat.replace("v", "'" + sTimeZoneShort + "'");
            } else {
                sTimeZoneShort = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("global.stdTimeZoneShort");
                sFormat = sFormat.replace("v", "'" + sTimeZoneShort + "'");
            }
            
            //Format date before timezone
            var sDate = DateFormat.getDateInstance({
                pattern: sFormat
            }).format(oValue);
            

            return sDate;
        }
    };
		
});