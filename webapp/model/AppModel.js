sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/DateFormat",
    "mysapui5app1/model/formatter"
], function (JSONModel, DateFormat, formatter) {
    "use strict";
    var AppModel = JSONModel.extend("mysapui5app1.model.AppModel", {
        constructor: function (oData){
            this.bLoaded = false;
            this.bFailed = false;
            
            JSONModel.apply(this, undefined);
            if (typeof oData === "string") {
                this.loadData(oData);
            }
            //this.setDefaultBindingMode("TwoWay");
        }
    });
    
    /**
     * Load JSON-encoded data from the server using a GET HTTP request and store the resulting JSON data in the model.
     * Note: Due to browser security restrictions, most "Ajax" requests are subject to the same origin policy,
     * the request can not successfully retrieve data from a different domain, subdomain, or protocol.
     *
     * @param {string} sURL A string containing the URL to which the request is sent.
     * @param {object | string} [oParameters] A map or string that is sent to the server with the request.
     * Data that is sent to the server is appended to the URL as a query string.
     * If the value of the data parameter is an object (map), it is converted to a string and
     * url-encoded before it is appended to the URL.
     * @param {boolean} [bAsync=true] By default, all requests are sent asynchronous
     * (i.e. this is set to true by default). If you need synchronous requests, set this option to false.
     * Cross-domain requests do not support synchronous operation. Note that synchronous requests may
     * temporarily lock the browser, disabling any actions while the request is active.
     * @param {string} [sType=GET] The type of request to make ("POST" or "GET"), default is "GET".
     * Note: Other HTTP request methods, such as PUT and DELETE, can also be used here, but
     * they are not supported by all browsers.
     * @param {boolean} [bMerge=false] whether the data should be merged instead of replaced
     * @param {string} [bCache=false] force no caching if false. Default is false
     * @param {object} [mHeaders] An object of additional header key/value pairs to send along with the request
     *
     * @public
     */
    AppModel.prototype.loadData = function(sURL, oParameters, bAsync, sType, bMerge, bCache, mHeaders){
        var pImportCompleted;

        bAsync = (bAsync !== false);
        sType = sType || "GET";
        bCache = bCache === undefined ? this.bCache : bCache;

        this.fireRequestSent({url : sURL, type : sType, async : bAsync, headers: mHeaders,
            info : "cache=" + bCache + ";bMerge=" + bMerge, infoObject: {cache : bCache, merge : bMerge}});

        var fnSuccess = function(oData) {
            if (!oData) {
                jQuery.sap.log.fatal("The following problem occurred: No data was retrieved by service: " + sURL);
            }
            this.setData(oData, bMerge);
            //delay the event so anyone can attach to this _before_ it is fired, but make
            //sure that bLoaded is already set properly
            this.bLoaded = true;
            this.bFailed = false;				
            this.fireRequestCompleted({url : sURL, type : sType, async : bAsync, headers: mHeaders,
                info : "cache=" + bCache + ";bMerge=" + bMerge, infoObject: {cache : bCache, merge : bMerge}, success: true});
        }.bind(this);

        var fnError = function(oParams){
            var oError = { message : oParams.textStatus, statusCode : oParams.request.status, statusText : oParams.request.statusText, responseText : oParams.request.responseText};
            jQuery.sap.log.fatal("The following problem occurred: " + oParams.textStatus, oParams.request.responseText + ","
                        + oParams.request.status + "," + oParams.request.statusText);
            this.bFailed = true;
            this.fireRequestCompleted({url : sURL, type : sType, async : bAsync, headers: mHeaders,
                info : "cache=" + bCache + ";bMerge=" + bMerge, infoObject: {cache : bCache, merge : bMerge}, success: false, errorobject: oError});
            this.fireRequestFailed(oError);
        }.bind(this);

        var _loadData = function(fnSuccess, fnError) {
            this._ajax({
                url: sURL,
                async: bAsync,
                dataType: 'json',
                cache: bCache,
                data: oParameters,
                headers: mHeaders,
                type: sType,
                success: fnSuccess,
                error: fnError
            });
        }.bind(this);

        if (bAsync) {
            pImportCompleted = new Promise(function(resolve, reject) {
                var fnReject =  function(oXMLHttpRequest, sTextStatus, oError) {
                    reject({request: oXMLHttpRequest, textStatus: sTextStatus, error: oError});
                };
                _loadData(resolve, fnReject);
            });

            this.pSequentialImportCompleted = this.pSequentialImportCompleted.then(function() {
                //must always resolve
                return pImportCompleted.then(fnSuccess, fnError).catch(function() {});
            });
        } else {
            _loadData(fnSuccess, fnError);
        }
    };		
    
    /**
     * Checks whether metadata is available
     *
     * @public
     * @returns {boolean} returns whether metadata is already loaded
     */
    AppModel.prototype.isLoaded = function() {
        return this.bLoaded;
    };
    
    /**
     * Checks whether metadata loading has already failed
     *
     * @public
     * @returns {boolean} returns whether metadata request has failed
     */
    AppModel.prototype.isFailed = function() {
        return this.bFailed;
    };	
    /**
     * Returns a promise which is fulfilled once the meta model data is loaded and can be used.
     *
     * @public
     * @returns {Promise} a Promise
     */
    AppModel.prototype.loaded = function () {
        return this.pSequentialImportCompleted;
    };			
    return AppModel;
}
);