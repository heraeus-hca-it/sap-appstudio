sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/core/format/DateFormat",
    "mysapui5app1/model/ViewModel",
    "mysapui5app1/model/formatter"
], function (Controller, History, DateFormat, ViewModel, formatter) {
    "use strict";
    return Controller.extend("mysapui5app1.controller.BaseController", {

        formatter: formatter,
        /**
         * Convenience method for accessing the message manager.
         * @public
         * @returns {sap.ui.core.MessageManager} the message manager for this component
         */			
        getMessageManager : function() {
            return sap.ui.getCore().getMessageManager();
        },
        /**
         * Convenience method for accessing the message model.
         * @public
         * @returns {sap.ui.core.MessageModel} the message model for this component
         */			
        getMessageModel : function() {
            return this.getMessageManager().getMessageModel();
        },
        /**
         * Convenience method for accessing the router.
         * @public
         * @returns {sap.ui.core.routing.Router} the router for this component
         */
        getRouter : function () {
            return sap.ui.core.UIComponent.getRouterFor(this);
        },
        /**
         * Convenience method for accessing the route parameters.
         * @public
         * @returns {sap.ui.core.routing.Router} the router for this component
         */		
        getRouteParameters: function() {
            return this._routeParameters;
        },
        /**
         * Convenience method for getting the component model by name.
         * @public
         * @param {string} [sName] the model name
         * @returns {sap.ui.model.Model} the model instance
         */
        getCompModel : function (sName) {
            if (!sName) {
                return this.getOwnerComponent().getModel();
            } else {
                return this.getOwnerComponent().getModel(sName);
            }
        },
        /**
         * Convenience method for getting the view model by name.
         * @public
         * @param {string} [sName] the model name
         * @returns {sap.ui.model.Model} the model instance
         */
        getViewModel : function (sName) {
            return this.getView().getModel(sName);
        },
        /**
         * Convenience method for setting the component model.
         * @public
         * @param {sap.ui.model.Model} oModel the model instance
         * @param {string} sName the model name
         * @returns {sap.ui.mvc.View} the view instance
         */
        setCompModel : function (oModel, sName) {
            return this.getOwnerComponent().setModel(oModel, sName);
        },
        /**
         * Convenience method for setting the view model.
         * @public
         * @param {sap.ui.model.Model} oModel the model instance
         * @param {string} sName the model name
         * @returns {sap.ui.mvc.View} the view instance
         */
        setViewModel : function (oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },
        /**
         * Getter for the resource bundle.
         * @public
         * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
         */
        getResourceBundle : function () {
            return this.getOwnerComponent().getModel("i18n").getResourceBundle();
        }


    });
}
);