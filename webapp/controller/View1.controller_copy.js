sap.ui.define([
    "sap/ui/core/mvc/Controller","mysapui5app1/controller/BaseController",
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     * @param {typeof mysapui5app1.controller.BaseController} BaseController
     */
    function (Controller,BaseController) {
        "use strict";

        return BaseController.extend("mysapui5app1.controller.View1", {
            onInit: function () {
                $.ajax({
                    url:"/XMII/Illuminator?service=admin&mode=CurrentProfile&Content-type=text/xml"
                    ,success:function(data){
                        console.log(data);
                        debugger;
                }});
            }
        });
    });
