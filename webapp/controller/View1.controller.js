sap.ui.define([
    "sap/ui/core/Fragment",    
    "mysapui5app1/controller/BaseController",
    "sap/ui/core/format/DateFormat"
],
    /**     
     * @param {typeof mysapui5app1.controller.BaseController} BaseController
     */
    function (Fragment,BaseController, DateFormat) {
        "use strict";

        return BaseController.extend("mysapui5app1.controller.View1", {
            onInit: function () {
                
                var oModel = new sap.ui.model.odata.v2.ODataModel("/XMII/IlluminatorOData/", {
                    "metadataUrlParams": {
                      "QueryTemplate": "HQS-HCA-VZUG-BubbleCount/IngotBubbles/sqlQueryRead",
                      "$format": "xml"
                    }
                });
                debugger;
                //this.getView().setModel(oModel);

                var oSmartTable = this.getView().byId("smartTable");
                //oSmartTable.setModel(oModel);
                
                /*
                https://sapui5.hana.ondemand.com/1.71.39/#/entity/sap.ui.comp.smarttable.SmartTable/sample/sap.ui.comp.sample.smarttable.mtableCustomToolbar/code/SmartTable.view.xml
                
                var oModel = this.getCompModel();                
                this.getView().setModel(oModel);
                debugger;
                */

                /*
                $.ajax({
                    url:"/XMII/Illuminator?service=admin&mode=CurrentProfile&Content-type=text/xml"
                    ,success:function(data){
                        console.log(data);
                        debugger;
                }});
                */
            },
            onExit : function () {
                if (this._oPopover) {
                    this._oPopover.destroy();
                }
            },            
            onTableUpdateFinished: function(oEvent){
                
            },
            onTableRefreshPress: function() {
                var oMachineSelect = this.getView().byId("machineSelect");
                
                var oMachineSelectedItem = oMachineSelect.getSelectedItem();
                
                var oTable = this.getView().byId("overviewTable");
                var oBindingInfo = oTable.getBindingInfo("items");
                if (!oBindingInfo.parameters) {
                    oBindingInfo.parameters = {};
                }
                if (!oBindingInfo.parameters.custom) {
                    oBindingInfo.parameters.custom = {};
                }
                var oCustomParams = oBindingInfo.parameters.custom;
                oCustomParams.DateColumn = "InspectionDatetimeUTC";
                oCustomParams.Dateformat = "yyyy-MM-dd'T'HH:mm:ss";
                var dEndDate = new Date();
                oCustomParams.EndDate = DateFormat.getDateInstance({
                    pattern: "yyyy-MM-ddTHH:mm:ss", UTC: true
                }).format(dEndDate);
                var dStartDate = new Date(dEndDate.getTime());
                dStartDate.setDate(dEndDate.getDate() - 1);
                oCustomParams.StartDate = DateFormat.getDateInstance({
                    pattern: "yyyy-MM-ddTHH:mm:ss", UTC: true
                }).format(dStartDate);				
                //oCustomParams.StartDate = "2020-07-17T14:12:00";
                //oCustomParams.EndDate = "2022-07-17T14:14:00";
                if (oMachineSelectedItem !== null){
                    var sMachineModel = oMachineSelect.getBindingInfo("items").model;
                    var oMachine = oMachineSelectedItem.getBindingContext(sMachineModel).getObject();
                    oCustomParams.FilterExpr = "[Machine]='" + oMachine.value + "'";
                } else {
                    oCustomParams.FilterExpr = "[Machine]=''";
                }
                oTable.bindItems(oBindingInfo);
                oTable.getBinding("items").refresh();
                oTable.getBinding("items").resume(); 
                
            },

    
            handleDateRangeValueHelp: function (oEvent) {
                var oMultiInput = oEvent.getSource();
    
                if (!this._oPopover) {
                    //var bizDateData = this.getView().byId("sFB").getControlByKey("businessdaydate").getBindingContext("$smartEntityFilter").getModel().getData(); 
                    //this.getView().byId("smartFilterBar").getControlByKey("InspectionDatetimeUTC").showItems();
                    this.getView().byId("smartFilterBar").getControlConfiguration()[1].setVisible(true);
                    this.getView().byId("smartFilterBar").getControlConfiguration()[2].setVisible(true);
                    debugger;
                    Fragment.load({
                        name: "mysapui5app1.fragment.DateRangePopover",
                        controller: this
                    }).then(function(oPopover){
                        this._oPopover = oPopover;
                        this.getView().addDependent(this._oPopover);
                        //this._oPopover.bindElement("/ProductCollection/0");
                        this._oPopover.openBy(oMultiInput);
                    }.bind(this));
                } else {
                    this._oPopover.openBy(oMultiInput);
                }
            },
    
            handleCloseButton: function (oEvent) {
                this._oPopover.close();
            }

        });
    });
